class Temperature
  attr_accessor :options, :fahrenheit, :celsius

  def initialize(options = {})
    defaults = {
      f: nil,
      c: nil
    }
    @options = defaults.merge(options)
    @fahrenheit = @options[:f]
    @celsius = @options[:c]
  end

  def in_celsius
    if @fahrenheit != nil
      (@fahrenheit - 32) * 5.0/9
    else
      @celsius
    end
  end

  def in_fahrenheit
    if @celsius != nil
      @celsius * 9/5.0 + 32
    else
      @fahrenheit
    end
  end

  def self.from_celsius(temp)
    Temperature.new(c: temp)
  end

  def self.from_fahrenheit(temp)
    Temperature.new(f: temp)
  end
end

class Celsius < Temperature
  attr_accessor :celsius

  def initialize(temp)
    @celsius = temp
  end
end

class Fahrenheit < Temperature
  attr_accessor :fahrenheit

  def initialize(temp)
    @fahrenheit = temp
  end
end
