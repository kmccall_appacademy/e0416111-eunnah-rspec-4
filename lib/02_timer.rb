class Timer

  attr_accessor :seconds

  def initialize
    @seconds = 0
  end

  def time_string
    hours = @seconds / 3600
    minutes = @seconds / 60 - (hours * 60)
    @seconds = @seconds % 60
    padded(hours) + ":" + padded(minutes) + ":" + padded(@seconds)
  end

  def padded(time)
    if time.to_s.length == 1
      return "0" + time.to_s
    else
      return time.to_s
    end
  end

end
