class Dictionary
  attr_accessor :entries, :keywords

  def initialize
    @entries = {}
  end

  def add(entry)
    if entry.class == String
      @entries[entry] = nil
    else
      @entries = @entries.merge(entry)
    end
  end

  def keywords
    words = @entries.keys.sort
  end

  def include?(keyword)
    @entries.has_key?(keyword)
  end

  def find(keyword)
    search_result = {}
    matches = keywords.select do |entry|
      entry.chars.take(keyword.length).join == keyword
    end
    matches.each { |word| search_result[word] = @entries[word] }
    search_result
  end

  def printable
    printable = []
    self.keywords.each do |key|
      printable << "[#{key}] \"#{@entries[key]}\""
    end
    printable.join("\n")
  end

end
