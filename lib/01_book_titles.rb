class Book

  attr_accessor :title

  def initialize
  end

  def title=(string)
    exceptions = ["the", "a", "an", "and", "in", "the", "of"]
    corrected = [string.split.first.capitalize]

    string.split.drop(1).each do |word|
      exceptions.include?(word)? corrected << word : corrected << word.capitalize
    end

    @title = corrected.join(" ")
  end
end
